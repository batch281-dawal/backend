/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];


    function register(username){

        let doesUserExist = registeredUsers.includes(username);

        if(doesUserExist){
            return "Registration Failed. Username already exists!";
        } else {
            registeredUsers.push(username);
            return "Thank you for registering!";
        }

    }




    function addFriend(username){



        let foundUser = registeredUsers.includes(username);

        if(foundUser){
            friendsList.push(username);
            return `You have added ${username} as a friend!`;
        } else {
            return "User not found.";
        }

    };



    
    function displayFriends(){


        if(friendsList.length > 0){

            friendsList.forEach(function(friend){
                console.log(friend);
            })


        } else {
           return "You have " + friendsList.length + " friends. Add one first.";
        }

    };





    function displayNumberOfFriends(){

        if(friendsList.length > 0){

            return "You currently have " + friendsList.length + " friends.";


        } else {
            return "You have " + friendsList.length + " friends. Add one first.";
        }

    };




    function deleteFriend(){

        if(friendsList.length > 0){

            friendsList.pop();

        } else {
            return "You have " + friendsList.length + " friends. Add one first."
        }

    };



function deleteSpecificFriend(username){


    if(friendsList.length > 0){

        let userIndex = friendsList.indexOf(username);
        //if indexOf() cannot find the item or cannot find the index of the item, it will return -1
        friendsList.splice(userIndex,1);

    } else {
        return "You have " + friendsList.length + " friends. Add one first."
    }

};










//Do not modify
//For exporting to test.js
try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend,
        deleteSpecificFriend
    }
}
catch(err) {

}

