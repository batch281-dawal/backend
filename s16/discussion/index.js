// JavaScript Operators

// Artihmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of substraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators (=) - assigns the vlue of the right operand to a variable

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Resule of addition assignment Operator: " + assignmentNumber);

// Shorthand 

assignmentNumber += 2;
console.log("Result of addition assignment Operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=. *=, /=)
assignmentNumber -= 2;
console.log("Result of subration assignment Operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment Operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment Operator: " + assignmentNumber);


// Multiple Operators and Parentheses 

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 -3) * (4 /5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2-3)) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement (++ , --)

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: "+ increment);
console.log("Result of pre-increment: "+ z);

// post-increment
increment = z++;
console.log("Result of pre-increment: "+ increment);
console.log("Result of pre-increment: "+ z);


let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: "+ z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: "+ z);

// Type Coercion 

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

// Comparison Operators

let karlos = 'juan';

// Equality Operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == karlos);

// Inequality operator (!=)

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != karlos);


// Strict Equality Operator (===)

console.log(1 ===  1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === karlos);

// Strict Inequality operator (!==)

console.log(1 !==  1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== karlos);

// Relational Operators 
let a = 50;
let b = 65;

// Greater than (>)
let isGreaterThan = a > b;
// Less than (<)
let isLessThan = a < b;
// Greater than or equal
let isGTorEqual = a >= b;
// Less than or equal (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;


// Logical and Operator (&&)
// Return true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operators: " + allRequirementsMet);

// Logical Or Operator (||)
// Return true if one of the operands are true 
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operators: " + someRequirementsMet);

// Logical Not Operator (!)
// Returns the opposite value 
let someRequirementsNotMet = !isRegistered;
console.log("Resule of logical NOT operator: " + someRequirementsNotMet);


















