const user = require("../models/User");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {

		// The find method returns a record if a match is found
		if(result.length > 0){
		return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	})
}