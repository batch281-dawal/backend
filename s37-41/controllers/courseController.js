const Course = require("../models/Course");
const User = require("../models/User");

// s39 - Activity 1
module.exports.addCourse = (reqBody) => {

		
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newCourse.save().then(course => true)
			.catch(err => false);
};

// s39 - Activity 2
/*module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return err
                } else {
                    //course creation successful
                    return true;
                }
            })
        }
        
    });    
}*/


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result)
		.catch(err => err);
};

module.exports.getAllActive = () => {

	return Course.find({ isActive : true }).then(result => result)
		.catch(err => err);
};


module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	}).catch(err => err);

};

module.exports.updateCourse = (reqParams, reqBody) => {


	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};


	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
		.then(course => true).catch(err => err);
};


// s40 - Activity 1
module.exports.archiveCourse = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(course => true).catch(err => err);
};

// s40 - Activity 2
/*module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(course => true).catch(err => err);
};*/
