const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");


// Route for checking if the email alreadt exists in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkemail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(
		resultFromController));
})

module.exports = router;