const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");

const app = express();

// Connect to our MongoDB
mongoose.connect("mongodb+srv://miguelleangelodawal:TKUGH8AwpskWNLrA@wdc028-course-booking.uldmdpn.mongodb.net/s37-41API?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB connection Error."));
db.once('open',() => console.log('Now connected to MongoDB Atlas!'));

// Allows all resources to access our backend applicaiton
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);

app.listen(process.env.PORT || 4000,() => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)

})




























