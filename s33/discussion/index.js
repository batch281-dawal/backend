// console.log("Hello World");

// [Section] Javascript Synchronous vs Asynchronous
// Javascript is by dafault synchronous meaning that only one statement is executed at a time

// This can be proven when a statement has an error, javascript will not proceed with the next statement
console.log("Hello");
//conosle.log("Hello Again!");
console.log("Goodbye");

// When certain statements takes a lot of time to process, this slow down our code
// An example of this are when loops are used on a large amout of information or when fetching data from databases'
// When an action wll take some time to process, this results in code "blocking"
// We might not notice it due to the fast procession power of our device
// This is the reason why some websites don't instantly load and we only see a white screen at times while the application is still waiting for all thhe code to be executed

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Create a simple fetch request 

// [Section] Getting all posts

// The fetch API allows you to asynchronously request for a resource(data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Check the status of the request 

// Retrieve all posts following the REST API(retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// The "fetch" method will return a "promise that resolves to a "Response" object
// The "then" method captures the "Response" on  object and returns another "promise" which will eventually be "Resolved" or "Rejected"
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "jason" method from the "Response" object to convert the data retrieved into JSON format to be used in our collection
.then((response) => response.json())
// Print the converted JSON value from the "fetch request"
// Using multiple "then" methods creates a promise chain 
.then((json) => console.log(json));

// Demonstrate using the "async" and "await" keywords

// The "async" and "await" keywords is another apprach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates asychronous function
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	// Result returned by fetch returns a promise
	console.log(result);
	// The returned "Response" is an object
	console.log(typeof result);
	// We cannot access the content of the "Response" by directly accessing its body's property
	console.log(result.body);

	// Converts data from the "Response" object as JSON
	let json = await result.json();
	// Print out the content of the "Response object"
	console.log(json);
};

fetchData();

// Process a GET request using postman

	/*	
		postman:
		url: https://jsonplaceholder.typicode.com/posts
		method GET
	*/
	
// [Section] Getting as specific post

// Retrieves a specific post following the REST API (retrieve, /posts/:id, GET)
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));
